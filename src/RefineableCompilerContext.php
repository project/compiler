<?php

namespace Drupal\compiler;

/**
 * A refineable compiler context used to define a compilation.
 *
 * Inputs can be modified using array access functionality provided by PHP.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class RefineableCompilerContext extends CompilerContext implements RefineableCompilerContextInterface {

  /**
   * {@inheritdoc}
   */
  public function offsetExists(mixed $offset): bool {
    return isset($this->inputs[$offset]);
  }

  /**
   * {@inheritdoc}
   */
  public function &offsetGet(mixed $offset): mixed {
    if (isset($this->inputs[$offset])) {
      return $this->inputs[$offset];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function offsetSet(mixed $offset, mixed $value): void {
    if (!is_null($offset)) {
      $this->inputs[$offset] = $value;
    }
    else {
      $this->inputs[] = $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function offsetUnset(mixed $offset): void {
    unset($this->inputs[$offset]);
  }

  /**
   * {@inheritdoc}
   */
  public function setCompiler(string $compiler): self {
    $this->compiler = $compiler;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setData($data): self {
    $this->data = $data;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOption(string $name, $value): self {
    $this->options[$name] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOptions(array $options): self {
    $this->options = $options;
    return $this;
  }

}
