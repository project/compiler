<?php

namespace Drupal\compiler;

/**
 * A compiler context used to define a compilation.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
interface CompilerContextInterface {

  /**
   * Get the machine name of the compiler plugin to use.
   *
   * @return string
   *   The machine name of the compiler plugin to use.
   */
  public function getCompiler(): string;

  /**
   * Get arbitrary user-defined data to provide to the compiler.
   *
   * @return mixed
   *   Arbitrary user-defined data to provide to the compiler.
   */
  public function getData();

  /**
   * Get an interator containing the compiler inputs.
   *
   * The resulting iterator will contain a copy of the input array at the time
   * when this method was invoked. The iterator will flatten the structure of
   * the input array and only return valid input values.
   *
   * For refineable contexts, this method should be used for retrieving inputs
   * unless the input array needs to be modified in some way.
   *
   * @return \RecursiveCallbackFilterIterator
   *   An interator containing the compiler inputs.
   */
  public function getInputs(): \RecursiveCallbackFilterIterator;

  /**
   * Get a specific compiler plugin configuration option.
   *
   * @param string $name
   *   The name of the option.
   *
   * @return mixed
   *   A specific compiler plugin configuration option.
   */
  public function getOption(string $name);

  /**
   * Get an associative array of compiler plugin configuration options.
   *
   * @return array
   *   An associative array of compiler plugin configuration options.
   */
  public function getOptions(): array;

}
