<?php

namespace Drupal\compiler;

/**
 * A compiler context used to define a compilation.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class CompilerContext implements CompilerContextInterface {

  /**
   * The machine name of the compiler plugin to use.
   *
   * @var string
   */
  protected $compiler = '';

  /**
   * Arbitrary user-defined data to provide to the compiler.
   *
   * @var mixed
   */
  protected $data = NULL;

  /**
   * An array of compiler inputs.
   *
   * @var \Drupal\compiler\CompilerInputInterface[]
   */
  protected $inputs = [];

  /**
   * An associative array of compiler plugin configuration options.
   *
   * @var array
   */
  protected $options = [];

  /**
   * Constructs a CompilerContext object.
   *
   * @param string $compiler
   *   The machine name of the compiler plugin to use.
   * @param array $options
   *   An associative array of compiler plugin configuration options.
   * @param \Drupal\compiler\CompilerInputInterface[] $inputs
   *   An array of compiler inputs.
   * @param mixed $data
   *   Arbitrary user-defined data to provide to the compiler.
   */
  public function __construct(string $compiler, array $options = [], array $inputs = [], $data = NULL) {
    // Store scalar/arbitrary data directly in instance properties.
    $this->compiler = $compiler;
    $this->data = $data;
    $this->inputs = $inputs;

    // Only allow associative keys for the outer-most level of options.
    $this->options = array_filter($options, function ($key) {
      return is_string($key);
    }, ARRAY_FILTER_USE_KEY);
  }

  /**
   * Create a filtered input iterator using the supplied compiler inputs.
   *
   * The resulting iterator will contain a copy of the inputs at the time when
   * this method was invoked.
   *
   * @param array $inputs
   *   An (optionally nested) array of compiler inputs.
   *
   * @return \RecursiveCallbackFilterIterator
   *   A filtered input iterator using the supplied compiler inputs.
   */
  protected function createIteratorForInputs(array $inputs): \RecursiveCallbackFilterIterator {
    $iterator = new \RecursiveArrayIterator($inputs, \RecursiveArrayIterator::CHILD_ARRAYS_ONLY);
    $iterator = new \RecursiveCallbackFilterIterator($iterator, function ($value, $key, $iterator) {
      return $iterator->hasChildren() || $value instanceof CompilerInputInterface;
    });

    return $iterator;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompiler(): string {
    return $this->compiler;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return $this->data;
  }

  /**
   * {@inheritdoc}
   */
  public function getInputs(): \RecursiveCallbackFilterIterator {
    return $this->createIteratorForInputs($this->inputs);
  }

  /**
   * {@inheritdoc}
   */
  public function getOption($name) {
    if (!array_key_exists($name, $this->options)) {
      return NULL;
    }

    return $this->options[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(): array {
    return $this->options;
  }

}
