<?php

namespace Drupal\compiler;

/**
 * A refineable compiler context interface.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
interface RefineableCompilerContextInterface extends \ArrayAccess, CompilerContextInterface {

  /**
   * Set the machine name of the compiler plugin to use for this context.
   *
   * @param string $compiler
   *   The machine name of the compiler plugin to use for this context.
   *
   * @return static
   *   The object itself for chaining.
   */
  public function setCompiler(string $compiler): self;

  /**
   * Set the user-defined data to use for this context.
   *
   * @param mixed $data
   *   The user-defined data to use for this context.
   *
   * @return static
   *   The object itself for chaining.
   */
  public function setData($data): self;

  /**
   * Set a specific option to use for this context.
   *
   * @param string $name
   *   The name of the option to set.
   * @param mixed $value
   *   The value to set for the option.
   *
   * @return static
   *   The object itself for chaining.
   */
  public function setOption(string $name, $value): self;

  /**
   * Set the options to use for this context.
   *
   * @param array $options
   *   The options to use for this context.
   *
   * @return static
   *   The object itself for chaining.
   */
  public function setOptions(array $options): self;

}
