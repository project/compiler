<?php

namespace Drupal\compiler\Plugin;

use Drupal\compiler\CompilerContextInterface;

/**
 * An interface that all compiler plugins should use.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
interface CompilerPluginInterface {

  /**
   * Compile the provided source context and return the result.
   *
   * Optional parameters are defined entirely by each concrete plugin
   * implementation and should be written as distinct parameters on this method.
   *
   * Officially-supported parameters will always be added to the context object.
   *
   * @param \Drupal\compiler\CompilerContextInterface $context
   *   The source context which defines or configures the compilation.
   *
   * @return mixed
   *   The result of compilation; an exception should be thrown instead of
   *   returning a result if an error state occurs within the compiler.
   */
  public function compile(CompilerContextInterface $context);

}
