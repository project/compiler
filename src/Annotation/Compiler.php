<?php

namespace Drupal\compiler\Annotation;

use Drupal\Component\Annotation\PluginID;

/**
 * Defines a compiler plugin annotation object.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @Annotation
 */
class Compiler extends PluginID {

}
