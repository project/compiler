<?php

namespace Drupal\compiler;

/**
 * A compiler input value that can be used directly for compilation.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class CompilerInputDirect extends CompilerInputBase {

}
